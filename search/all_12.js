var searchData=
[
  ['s0_5finit_0',['S0_INIT',['../task_i_m_u_8py.html#a46cc6139e9e767d8cdddb4b56c1f0efc',1,'taskIMU']]],
  ['s1_5fcalib_1',['S1_CALIB',['../task_i_m_u_8py.html#a8fc7b05ac3d0fbe6a6fa32d76bcbeca5',1,'taskIMU']]],
  ['s1_5fstay_2',['S1_STAY',['../task_motor_8py.html#a51e9955da618469fc79f9d28797515f3',1,'taskMotor']]],
  ['s2_5fread_3',['S2_READ',['../task_i_m_u_8py.html#a1f964b745d4141e6e2ae2cf554433eeb',1,'taskIMU']]],
  ['sawwave_4',['SawWave',['../_lab0x01_8py.html#ae9efcfcb4bc80c8e803d438834211430',1,'Lab0x01']]],
  ['set_5fduty_5',['set_duty',['../classmotor_1_1_motor.html#a08f41a32c8b122a8b94496fef2a0a901',1,'motor.Motor.set_duty()'],['../classmotor3_1_1_motor.html#a173795d52b17e03c92f67b37811ea842',1,'motor3.Motor.set_duty()'],['../classmotor4_1_1_motor.html#a566720341be4cfe7c90bd4b4f1cfa578',1,'motor4.Motor.set_duty()'],['../classmotor5_1_1_motor.html#a840f195d9f6f0345ec6fe72e2d2a2318',1,'motor5.Motor.set_duty()']]],
  ['set_5fgain_6',['set_gain',['../classclosed_loop_1_1_closed_loop.html#af0b6484433414b73559855334640b31b',1,'closedLoop::ClosedLoop']]],
  ['share_7',['Share',['../classshares_1_1_share.html',1,'shares']]],
  ['shares_2epy_8',['shares.py',['../shares_8py.html',1,'']]],
  ['simulation_20of_202d_20ball_20and_20platform_20model_9',['Simulation of 2D Ball and Platform Model',['../hw3.html',1,'index']]],
  ['sinewave_10',['SineWave',['../_lab0x01_8py.html#a59053514da7bc2b75fda87e59ec09a31',1,'Lab0x01']]],
  ['squarewave_11',['SquareWave',['../_lab0x01_8py.html#af17099176c965f623e873b54a860cb5c',1,'Lab0x01']]],
  ['starttime_12',['startTime',['../_lab0x01_8py.html#a5bcf26f4ba199c914e2089e256e04da1',1,'Lab0x01']]],
  ['state_13',['state',['../_lab0x01_8py.html#af49dfbadfaec711466de0f806232bf66',1,'Lab0x01']]]
];
